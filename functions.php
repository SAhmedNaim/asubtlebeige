<?php

add_theme_support( 'title-tag' );

$default_custom_image = array(
	'default-image'	=> get_template_directory_uri().'/images/header.png'
);
add_theme_support( 'custom-header', $default_custom_image );

add_theme_support( 'custom-background' );

add_theme_support( 'post-thumbnails' );

// loading textdomain
load_theme_textdomain( 'subtle', get_template_directory() . '/languages' );

// menu register
register_nav_menus(array(
	'main-menu'		=> __('Main Menu', 'subtle'),
	'footer-menu'	=> __('Footer Menu', 'subtle'),
));
